# Alfredo Garcia - Supplemental benefactor - 1.14.110

Author: oxarbitrage

https://github.com/oxarbitrage/

## Motivation

In the last 6 months benefactor for Alfredo Garcia in regards to development in the core and other areas(https://github.com/oxarbitrage/benefactor-proposals/blob/master/dxpchain2018_1.md) was failing all the period in making the funds to cover the benefactor costs.

DXP price at the moment of creating benefactor was: 0,769230769 USD. Even with protection of 2.5 multiplier DXP was always below a limit of 0,30 USD. This is the price needed to cover this benefactor as it was created.

Benefactor was for 6 months at 6660 usd per month. 

This is a supplemental benefactor created to cover the debt, pay everything and end the contract. 

Alfredo Garcia, is not leaving Dxpchain, on the contrary it is joining the dxpchain-core team benefactor proposal.

## Transfers made inside this period:

The escrow of this benefactor only pulled money from the blockchain 3 times in the 6 months period:

alfredo-benefactor sent 4,800 USD to oxarbitrage.a699 1.11.151678027

alfredo-benefactor sent 3,000 USD to oxarbitrage.a699 1.11.167111927

alfredo-benefactor sent 5,998 USD to oxarbitrage.a699 1.11.196057968

Payments made until now:

6000 + 4800 + 3000 = 13,800

39,600(total 6 months) - 13,800(paid until now) = 25,800 USD of debt today.

## Current vesting:

Benefactor haves some DXP accumulated to pay part of the debt in vesting balances:

1,072.788 DXP - 1.13.1609 , 91.125 DXP - 1.13.1608 , 39,414.367 DXP - 1.13.4839 (around 40,000 DXP total in vesting)

If we buy usd today with what we have: 40,000 dxp x 0,18(dxp/usd price) = 7200 usd

Debt if we do that: 25,800 - 7,200 = 18,600

## Ending 2 weeks earlier:

The core team has proposed to end this benefactor earlier and join the core benefactor immediately, 2 weeks before the end of the contract.

means, 6600/2 = 3300 usd for the last month.

so instead of 39,600 the total for the benefactor is now 36,300 usd.

the 3 payments made:

6000 + 4800 + 3000 = 13,800

36300 - 13800 = 22200

assuming we buy 7200 usd today: 22200 - 7200 = 14,800 debt.

## Benefactor payment and duration:

We want to collect the needed funds and end at around the same time the initial benefactor was supposed to finish, pay all the debt and burn back what left.

`alfredo-benefactor` is an escrow account formed by the following dxpchain community members(unchanged):
```
chainsquad
dev.dxpchainblocks
elmato
fox
lin9uxis
```
Start date: 2018-07-15

End Date: 2018-07-20

DXP Per day: 20000 DXP

At current rates of 0,18 USD per DXP supplemental benefactor will collect 100000 DXP that will be 18,000 USD. We only need 14,800 usd at today rates but we need to cover in case DXP price drops below.
All non used DXP will be burned back to the chain, escrow group is fully compromised on this.

The supplemental benefactor proposal was considered since the beggining at: https://github.com/oxarbitrage/benefactor-proposals/blob/master/dxpchain2018_1.md#notes

The expiring benefactor was the most voted benefactor proposal in the history of dxpchain, we hope the stakeholders that trusted on it  before understand the importance of this and approve this supplemental benefactor to cover the debt, this is a benefactor that we tried to avoid at all cost speculating on a recovery of the price that never happened.


