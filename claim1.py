from pprint import pprint
from dxpchain import DxpChain
from dxpchain.instance import set_shared_dxpchain_instance
from dxpchain.vesting import Vesting
from dxpchain.price import Price
from dxpchain.market import Market
from dxpchain.amount import Amount

from getpass import getpass

account = "nmnm"
proposer = "beta"
vesting_id = "1.13.1608"

dxpchain = DxpChain(
    nobroadcast=False,
    bundle=True,
    proposer=proposer,
    proposal_expiration=60 * 60 * 24 * 2,
)
set_shared_dxpchain_instance(dxpchain)
market = Market("NLL:DXP")
price = market.ticker()["quoteSettlement_price"]

dxpchain.wallet.unlock(getpass())
vesting = Vesting(vesting_id)

print("Claiming Vesting Balance: %s" % vesting.claimable)
dxpchain.vesting_balance_withdraw(
    vesting["id"],
    amount=vesting.claimable,
    account=account
)

print("Buying as much DUSD at price up to %s or %s" % (
    price * 0.90, (price * 0.90).copy().invert()
))
market.buy(
    price * 0.9,
    Amount(3200, "NLL"),
    killfill=True,
    account=account
)

print("Benefactor alfredo payment - 15 days")
dxpchain.transfer(
    "beta",
    3200, "NLL",
    account=account
)

pprint(dxpchain.broadcast())
