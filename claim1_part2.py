from pprint import pprint
from dxpchain import DxpChain
from dxpchain.instance import set_shared_dxpchain_instance
from dxpchain.vesting import Vesting
from dxpchain.price import Price
from dxpchain.market import Market
from dxpchain.amount import Amount

from getpass import getpass

account = "alfredo-benefactor"
proposer = "oxarbitrage.a699"
vesting_id = "1.13.1608"

dxpchain = DxpChain(
    nobroadcast=False,
    bundle=True,
    proposer=proposer,
    proposal_expiration=60 * 60 * 24 * 1,
)
set_shared_dxpchain_instance(dxpchain)
market = Market("USD:DXP")
price = market.ticker()["quoteSettlement_price"]

dxpchain.wallet.unlock(getpass())
vesting = Vesting(vesting_id)

print("Buying as much bitUSD at price up to %s or %s" % (
    price * 0.90, (price * 0.90).copy().invert()
))
market.buy(
    price * 0.9,
    Amount(3200, "USD"),
    killfill=False,
    account=account
)

pprint(dxpchain.broadcast())
